package vn.hblab.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class DateSchedule extends RealmObject {
    @PrimaryKey
    private String date;

    public DateSchedule(String date) {
        this.date = date;
    }

    public DateSchedule() {
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
