package vn.hblab.compactCalendar;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.github.sundeepk.compactcalendarview.CompactCalendarView;
import com.github.sundeepk.compactcalendarview.domain.Event;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import java.io.File;
import java.io.FileReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;
import io.realm.RealmResults;
import vn.hblab.loginmaterial.TestActivity;
import vn.hblab.model.DateSchedule;
import vn.hblab.model.Model;
import vn.hblab.model.User;
import vn.hblab.schedule.LessonAdapter;
import vn.hblab.skma.R;

public class CalendarActivity extends AppCompatActivity {

    private static final String TAG = "CalendarActivity";
    @BindView(R.id.view10)
    View view10;
    @BindView(R.id.tx_today)
    TextView txToday;
    @BindView(R.id.text_item)
    TextView textItem;
    private String dateFormatDay = "dd/MM/yyyy";
    @BindView(R.id.btn_exit)
    ImageButton btnExit;
    @BindView(R.id.ln_toolbar)
    LinearLayout lnToolbar;
    @BindView(R.id.text)
    TextView textDate;
    @BindView(R.id.compactcalendar_view)
    CompactCalendarView compactcalendarView;
    @BindView(R.id.list_recycleView)
    RecyclerView listRecycleView;
    private LessonAdapter adapter;
    private List<Model.Schedule> datas;
    private File file;
    private File gpxfile;
    private FileReader reader;
    private Realm mRealm;
    private Realm mRealmDate;
    private RealmResults<DateSchedule> dateSchedules;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar);
        ButterKnife.bind(this);
        Realm.init(this);
        mRealm = Realm.getDefaultInstance();
        mRealmDate = Realm.getDefaultInstance();

        RealmResults<User> user = mRealm.where(User.class).findAll();

        dateSchedules = mRealmDate.where(DateSchedule.class).findAll();

        readFileToIS(this, "data.json");

        calendarView();

        btnExit.setOnClickListener(v -> {
            mRealm.beginTransaction();
            mRealm.delete(User.class);
            mRealm.commitTransaction();
            mRealm.close();
            file.delete();
            gpxfile.delete();
            Intent goLogin = new Intent(this, TestActivity.class);
            startActivity(goLogin);
            finish();
        });


    }

    private void calendarView() {
        compactcalendarView.setFirstDayOfWeek(Calendar.MONDAY);

        Calendar calendar = Calendar.getInstance();

        String dateToday = (String) DateFormat.format(dateFormatDay, calendar.getTime());

        txToday.setText("Lịch học ngày " + DateFormat.format("dd MMM yyyy", calendar.getTime()));

        List<Event> eventList = new ArrayList<>();

        for (Model.Schedule model : datas) {
            compactcalendarView.addEvent(new Event(Color.RED, Long.valueOf(model.getDate())));
            if (model.getLessons().size() > 1) {
                eventList.add(new Event(Color.RED, Long.valueOf(model.getDate())));
            }
            compactcalendarView.addEvents(eventList);
        }

        textDate.setText(DateFormat.format("MMM yyyy", calendar.getTime()));

        setAdapterCalender(dateToday);

        compactcalendarView.addEvents(eventList);

        compactcalendarView.setShouldDrawDaysHeader(false);

        compactcalendarView.setListener(new CompactCalendarView.CompactCalendarViewListener() {
            @Override
            public void onDayClick(Date dateClicked) {
                String timeStamp = new SimpleDateFormat(dateFormatDay).format(dateClicked.getTime());
                txToday.setText("Lịch học ngày " + DateFormat.format("dd MMM yyyy", dateClicked.getTime()));
                textDate.setText(DateFormat.format("MMM yyyy", dateClicked.getTime()));
                Log.d(TAG, "onDayClick: " + timeStamp);
//                readFileToIS(getApplicationContext(), "data.json");
                setAdapterCalender(timeStamp);
            }

            @Override
            public void onMonthScroll(Date firstDayOfNewMonth) {
                textDate.setText(DateFormat.format("MMM yyyy", firstDayOfNewMonth.getTime()));
            }
        });
    }

    private void setAdapterCalender(String date) {
        for (Model.Schedule schedule : datas) {
            if (convertDate(schedule.getDate(), dateFormatDay).equals(date)) {
                textItem.setVisibility(View.GONE);
                listRecycleView.setVisibility(View.VISIBLE);
                setAdapterLession(schedule.getLessons());
                return;
            } else {
                listRecycleView.setVisibility(View.GONE);
                textItem.setVisibility(View.VISIBLE);
            }
        }
    }

    private void setAdapterLession(List<Model.Schedule.Lesson> lessons) {
        adapter = new LessonAdapter(lessons);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        listRecycleView.setLayoutManager(layoutManager);
        listRecycleView.setItemAnimator(new DefaultItemAnimator());
        listRecycleView.setAdapter(adapter);
    }

    private void readFileToIS(Context context, String fileName) {
        file = new File(context.getFilesDir(), "mydir");
        if (!file.exists()) {
            file.mkdir();
        }
        reader = null;
        try {
            gpxfile = new File(file, fileName);

            reader = new FileReader(gpxfile);

        } catch (Exception e) {
            e.printStackTrace();

        }

        Gson gson = new GsonBuilder().create();
        JsonElement jsonElement = new JsonParser().parse(reader);
        Model model = gson.fromJson(jsonElement, Model.class);
        if (model != null) {

            datas = model.getSchedule();

            Collections.sort(datas, (o1, o2) -> convertDate(o1.getDate(), "MM/dd/yyyy '@'hh:mm a").compareTo(convertDate(o2.getDate(), "MM/dd/yyyy '@'hh:mm a")));
        }
    }

    public static String convertDate(String dateInMilliseconds, String dateFormat) {
        return DateFormat.format(dateFormat, Long.parseLong(dateInMilliseconds)).toString();
    }
}
