package vn.hblab.skma;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import vn.hblab.login.AccLoginFrag;
import vn.hblab.schedule.ScheduleActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        getSupportFragmentManager().beginTransaction().replace(R.id.container, AccLoginFrag.newInstance(this)).commit();
        Intent intent = new Intent(this, ScheduleActivity.class);
        startActivity(intent);
    }
}
