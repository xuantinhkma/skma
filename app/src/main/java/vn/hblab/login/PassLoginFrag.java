package vn.hblab.login;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.gson.Gson;

import java.io.Serializable;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import io.realm.DynamicRealm;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmConfiguration.Builder;
import io.realm.RealmMigration;
import io.realm.RealmResults;
import io.realm.RealmSchema;
import vn.hblab.model.Model;
import vn.hblab.model.User;
import vn.hblab.network.ApiClient;
import vn.hblab.network.ApiService;
import vn.hblab.schedule.ScheduleActivity;
import vn.hblab.skma.MainActivity;
import vn.hblab.skma.R;

public class PassLoginFrag extends Fragment {

    private ProgressDialog mProgressDialog;
    MainActivity activity;
    String username;
    @BindView(R.id.edt_pass)
    EditText edtPass;
    @BindView(R.id.btn_pass_login)
    ImageButton btnPassLogin;
    @BindView(R.id.tx_noti)
    TextView txNoti;
    private Realm mRealm;

    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private ApiService apiService;


    public static PassLoginFrag newInstance(MainActivity activity, String username) {
        Bundle bundle = new Bundle();
        PassLoginFrag frag = new PassLoginFrag();
        frag.username = username;
        frag.activity = activity;
        frag.setArguments(bundle);
        return frag;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mProgressDialog = new ProgressDialog(activity);
        mProgressDialog.setMessage("Doing something, please wait.");

        btnPassLogin.setEnabled(false);
        txNoti.setVisibility(View.GONE);
        edtPass.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                txNoti.setVisibility(View.VISIBLE);
                btnPassLogin.setEnabled(true);
                txNoti.setText("Chọn button bên phải để tiếp tục");
                btnPassLogin.setOnClickListener(v -> {
                    Realm.init(activity);
                    RealmConfiguration config = new RealmConfiguration
                            .Builder()
                            .schemaVersion(0)
                            .migration(new MyMigration())
                            .build();
//                    Realm.setDefaultConfiguration(config);
                    mRealm = Realm.getDefaultInstance();
                    onLogin(username, s.toString());
                });
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void onLogin(String username, String password) {
        mProgressDialog.show();
        apiService = ApiClient.getClient(activity).create(ApiService.class);
        String passwordhash = "md5";
        String fbclid = "IwAR0PCNzGINHaFkXuVtzs9CBybYtZzuDMawmRyUqee8wL-z4xlCNMDruWvTM";
        compositeDisposable
                .add(apiService.getLogin(username.toUpperCase(), password, passwordhash, fbclid)
                        .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(new DisposableSingleObserver<Model>() {
                            @Override
                            public void onSuccess(Model model) {

                                Log.d("0o0o", "onSuccess: " + new Gson().toJson(model));

                                if (model.getSuccess() == null){
                                    txNoti.setText("Tài khoản hoặc mật khẩu không chính xác!");
                                    return;
                                }

                                User user = new User();
                                user.setUsername(username.toUpperCase());
                                user.setPassword(password);
                                mRealm.beginTransaction();
                                mRealm.copyToRealmOrUpdate(user);
                                mRealm.commitTransaction();
                                mRealm.close();



                                Intent intent = new Intent(activity, ScheduleActivity.class);
                                intent.putExtra("username", username.toUpperCase());
                                intent.putExtra("password", password);
                                startActivity(intent);
                                mProgressDialog.dismiss();
                            }

                            @Override
                            public void onError(Throwable e) {
                                txNoti.setText(e.getMessage());
                            }
                        }));
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.pass_login_frag, container, false);
        ButterKnife.bind(this, view);
        return view;
    }


    private class MyMigration implements RealmMigration {

        @Override
        public void migrate(@NonNull DynamicRealm realm, long oldVersion, long newVersion) {
            // DynamicRealm exposes an editable schema
            RealmSchema schema = realm.getSchema();
        }
    }
}
