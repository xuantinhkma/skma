package vn.hblab.login;

import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import vn.hblab.skma.MainActivity;
import vn.hblab.skma.R;

public class AccLoginFrag extends Fragment {

    MainActivity activity;
    @BindView(R.id.edt_user_name)
    EditText edtUserName;
    @BindView(R.id.btn_acc_login)
    ImageButton btnAccLogin;
    @BindView(R.id.cv_acc)
    CardView cvAcc;
    @BindView(R.id.tx_noti)
    TextView txNoti;

    public static AccLoginFrag newInstance(MainActivity activity) {
        Bundle bundle = new Bundle();
        AccLoginFrag frag = new AccLoginFrag();
        frag.activity = activity;
        frag.setArguments(bundle);
        return frag;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        btnAccLogin.setEnabled(false);
        txNoti.setVisibility(View.GONE);
        edtUserName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                txNoti.setVisibility(View.VISIBLE);
                btnAccLogin.setEnabled(true);
                if (s.length() == 8) {
                    txNoti.setTextColor(Color.parseColor("#7E7E7E"));
                    cvAcc.setCardBackgroundColor(Color.parseColor("#0078D7"));
                    txNoti.setText("Chọn button bên phải để tiếp tục");
                    btnAccLogin.setOnClickListener(v -> {
                        Toast.makeText(activity, "Enabled button", Toast.LENGTH_SHORT).show();
                        activity.getSupportFragmentManager().beginTransaction().replace(R.id.container, PassLoginFrag.newInstance(activity, s.toString())).commit();
                    });
                } else {
                    txNoti.setTextColor(Color.parseColor("#FFFF1744"));
                    txNoti.setText("Tài khoản không đúng");
                    cvAcc.setCardBackgroundColor(Color.parseColor("#AAAAAA"));
                    btnAccLogin.setEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.acc_login_frag, container, false);
        ButterKnife.bind(this, view);
        return view;
    }
}
