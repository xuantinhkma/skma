package vn.hblab.loginmaterial;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import vn.hblab.skma.R;

public class TestActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_main);
        getSupportFragmentManager().beginTransaction().replace(R.id.container, LoginFrag.newInstance(this)).commit();
    }
}
