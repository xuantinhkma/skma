package vn.hblab.loginmaterial;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.Gson;

import java.io.File;
import java.io.FileWriter;

import butterknife.BindView;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import vn.hblab.compactCalendar.CalendarActivity;
import vn.hblab.login.PassLoginFrag;
import vn.hblab.model.Model;
import vn.hblab.model.User;
import vn.hblab.network.ApiClient;
import vn.hblab.network.ApiService;
import vn.hblab.schedule.ScheduleActivity;
import vn.hblab.skma.R;

public class LoginFrag extends Fragment {

    ImageButton btnPassLogin;
    private ProgressDialog mProgressDialog;
    TextInputEditText edtUserName;
    TextInputEditText edtPass;
    private TestActivity activity;
    private Realm mRealm;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private ApiService apiService;

    public static LoginFrag newInstance(TestActivity activity) {
        Bundle bundle = new Bundle();
        LoginFrag loginFrag = new LoginFrag();
        loginFrag.activity = activity;
        loginFrag.setArguments(bundle);
        return loginFrag;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.acc_materia_fragment, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        edtPass = activity.findViewById(R.id.edt_pass);
        btnPassLogin = activity.findViewById(R.id.btn_pass_login);
        edtUserName = activity.findViewById(R.id.edt_user_name);
        mProgressDialog = new ProgressDialog(activity);
        mProgressDialog.setMessage("Doing something, please wait.");
        edtPass.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                btnPassLogin.setOnClickListener(v -> {
                    String username = edtUserName.getText().toString().toUpperCase();
                    String password = s.toString();
                    Realm.init(activity);
                    mRealm = Realm.getDefaultInstance();
                    onLogin(username, password);
                });
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    private void onLogin(String username, String password) {
        mProgressDialog.show();
        apiService = ApiClient.getClient(activity).create(ApiService.class);
        String passwordhash = "md5";
        String fbclid = "IwAR0PCNzGINHaFkXuVtzs9CBybYtZzuDMawmRyUqee8wL-z4xlCNMDruWvTM";
        compositeDisposable
                .add(apiService.getLogin(username, password, passwordhash, fbclid)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(new DisposableSingleObserver<Model>() {
                            @Override
                            public void onSuccess(Model model) {
                                if (model.getSuccess() == null){
                                    return;
                                }

                                Log.d("0o0o", "onSuccess: " + new Gson().toJson(model));
                                writeFileToIS(activity, "data.json", new Gson().toJson(model));


                                User user = new User();
                                user.setUsername(username);
                                user.setPassword(password);
                                mRealm.beginTransaction();
                                mRealm.copyToRealmOrUpdate(user);
                                mRealm.commitTransaction();
                                mRealm.close();

                                Intent intent = new Intent(activity, CalendarActivity.class);
                                startActivity(intent);
                                mProgressDialog.dismiss();
                            }

                            @Override
                            public void onError(Throwable e) {
                            }
                        }));
    }

    private void writeFileToIS(Context context, String fileName, String body) {
        File file = new File(context.getFilesDir(),"mydir");
        if(!file.exists()){
            file.mkdir();
        }

        try{
            File gpxfile = new File(file, fileName);
            FileWriter writer = new FileWriter(gpxfile);
            writer.append(body);
            writer.flush();
            writer.close();
            Log.d("0o0o", "writeFileToIS");

        }catch (Exception e){
            e.printStackTrace();

        }
    }
}
