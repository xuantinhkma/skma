package vn.hblab.schedule;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import io.realm.Realm;
import io.realm.RealmResults;
import vn.hblab.loginmaterial.TestActivity;
import vn.hblab.model.DateSchedule;
import vn.hblab.model.Model;
import vn.hblab.model.User;
import vn.hblab.network.ApiClient;
import vn.hblab.network.ApiService;
import vn.hblab.skma.R;

public class ScheduleActivity extends AppCompatActivity implements ScheduleAdapter.ListItemClickListener {

    private static final String TAG = "0o0o";
    @BindView(R.id.btn_exit)
    ImageButton btnExit;
    @BindView(R.id.tv_name)
    TextView tvName;
    @BindView(R.id.tv_mssv)
    TextView tvMssv;
    @BindView(R.id.tv_nghanh)
    TextView tvNghanh;
    @BindView(R.id.tv_class)
    TextView tvClass;
    @BindView(R.id.rcv_schedule)
    RecyclerView rcvSchedule;
    @BindView(R.id.txt_inforday)
    TextView txtInforday;
    @BindView(R.id.rcv_lesson)
    RecyclerView rcvLesson;
    @BindView(R.id.btn_home)
    ImageButton btnHome;
    @BindView(R.id.cv_home)
    CardView cvHome;
    private List<Model.Schedule> data = new ArrayList<>();
    private String username;
    private String password;
    private ProgressDialog mProgressDialog;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private ApiService apiService;
    private LessonAdapter lessonRvAdapter;
    private ScheduleAdapter mAdapter;
    private Realm mRealm;
    private Realm mRealmDate;
    private String mili;
    private LinearLayoutManager managerLesstion;
    private int indexDate = 0;
    private File file;
    private File gpxfile;
    private FileReader reader;
    private RealmResults<DateSchedule> dateSchedules;

    @SuppressLint("ResourceType")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schedule);
        ButterKnife.bind(this);
        Realm.init(this);
        mRealm = Realm.getDefaultInstance();
        mRealmDate = Realm.getDefaultInstance();

        RealmResults<User> user = mRealm.where(User.class).findAll();
        dateSchedules = mRealmDate.where(DateSchedule.class).findAll();

        for (User user1 : user) {
            username = user1.getUsername();
            password = user1.getPassword();
        }

        Log.d("0o0o", "onCreate: " + username + password);

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage("Doing something, please wait.");
        initView();

        readFileToIS(this, "data.json");

        initView();
        onSuccessSch();

//        loadCities();
//        data = loadCities();

//        onSchedule();

        btnExit.setOnClickListener(v -> {
            mRealm.beginTransaction();
            mRealm.delete(User.class);
            mRealm.commitTransaction();
            mRealm.close();
            file.delete();
            gpxfile.delete();
            Intent goLogin = new Intent(this, TestActivity.class);
            startActivity(goLogin);
            finish();
        });

    }

    private void readFileToIS(Context context, String fileName) {
        file = new File(context.getFilesDir(),"mydir");
        if(!file.exists()){
            file.mkdir();
        }
        reader = null;
        try{
            gpxfile = new File(file, fileName);

            reader = new FileReader(gpxfile);

            Log.d("0o0o", "writeFileToIS");

        }catch (Exception e){
            e.printStackTrace();

        }

        Gson gson = new GsonBuilder().create();
        JsonElement jsonElement = new JsonParser().parse(reader);
        Model model = gson.fromJson(jsonElement, Model.class);
        if (model != null) {
            mProgressDialog.dismiss();
            tvClass.setText("Lớp: " + model.getClassName());
            tvName.setText("Sinh Viên: " + model.getName());
            tvMssv.setText("MSSV: " + model.getStudentId());
            tvNghanh.setText("Ngành: " + model.getMajor());
            data = model.getSchedule();
            onSuccessSch();
        }
    }

    private void initView() {
        mAdapter = new ScheduleAdapter(data, this, this);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false);
        rcvSchedule.setLayoutManager(layoutManager);
        rcvSchedule.setItemAnimator(new DefaultItemAnimator());
        rcvSchedule.setAdapter(mAdapter);
    }

    private void onSuccessSch() {
        Collections.sort(data, (o1, o2) -> convertDate(o1.getDate(), "MM/dd/yyyy '@'hh:mm a").compareTo(convertDate(o2.getDate(), "MM/dd/yyyy '@'hh:mm a")));
        String timeStamp = new SimpleDateFormat("MM/dd/yyyy").format(Calendar.getInstance().getTime());
        mili = timeStamp;
        Log.d(TAG, "onSchedule: " + mili);
        for (int i = 0; i < data.size(); i++) {
            if (convertDate(data.get(i).getDate(), "MM/dd/yyyy").equals(mili)) {
                Log.d(TAG, "onSuccessSch: " + i);
                data.get(i).setClicked(true);
                indexDate = i;
                mRealmDate.beginTransaction();
                mRealmDate.delete(DateSchedule.class);
                mRealmDate.commitTransaction();
                mRealmDate.close();

            }
        }

        mAdapter = new ScheduleAdapter(data, position -> {
            for (Model.Schedule schedule : data) {
                int index = data.indexOf(schedule);
                if (index == position) {
                    schedule.setClicked(true);
                } else {
                    schedule.setClicked(false);
                }
            }

            if (position != indexDate){
                cvHome.setVisibility(View.VISIBLE);
                btnHome.setVisibility(View.VISIBLE);
                btnHome.setOnClickListener(v->{
                    data.get(indexDate).setClicked(true);
                    data.get(position).setClicked(false);
                    setLesson(indexDate);
                    setSchedule(indexDate);
                });
            } else {
                cvHome.setVisibility(View.GONE);
                btnHome.setVisibility(View.GONE);
            }
            setLesson(position);
        }, this);

        cvHome.setVisibility(View.GONE);
        btnHome.setVisibility(View.GONE);

        setSchedule(indexDate);
    }

    public static String convertDate(String dateInMilliseconds, String dateFormat) {
        return DateFormat.format(dateFormat, Long.parseLong(dateInMilliseconds)).toString();
    }

    private void setSchedule(int position) {
        setLesson(position);
        LinearLayoutManager managerSchedule = new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false);
        managerSchedule.scrollToPositionWithOffset(position, rcvSchedule.getWidth() / 2);
        rcvSchedule.setLayoutManager(managerSchedule);
        rcvSchedule.setItemAnimator(new DefaultItemAnimator());
        rcvSchedule.setAdapter(mAdapter);
    }

    private void setLesson(int position) {
        mAdapter.notifyDataSetChanged();
        txtInforday.setText("Bạn đang xem lịch học ngày " + convertDate(data.get(position).getDate(), "dd/MM/yyyy"));
        lessonRvAdapter = new LessonAdapter(data.get(position).getLessons());
        managerLesstion = new LinearLayoutManager(ScheduleActivity.this, RecyclerView.VERTICAL, false);
        rcvLesson.setLayoutManager(managerLesstion);
        rcvLesson.setItemAnimator(new DefaultItemAnimator());
        rcvLesson.setAdapter(lessonRvAdapter);
    }

    @Override
    public void onItemClick(int position) {

    }

//    private List<Model.Schedule> loadCities() {
//        // In this case we're loading from local assets.
//        // NOTE: could alternatively easily load from network.
//        // However, that would need to happen on a background thread.
////        FileInputStream stream;InputStreamReader isr
//        InputStreamReader isr;
//        try {
//            FileInputStream fis = this.openFileInput("hello.txt");
//            isr = new InputStreamReader(fis);
////            stream = this.openFileInput("data.json");
//            Log.d(TAG, "loadCities: ");
//        } catch (IOException e) {
//            return null;
//        }
//
//        Gson gson = new GsonBuilder().create();
//
////        JsonElement json = new JsonParser().parse(new InputStreamReader(stream));
//        JsonElement json = new JsonParser().parse(isr);
//
//        Model model = gson.fromJson(json, Model.class) ;
//
//        if (model != null) {
//            mProgressDialog.dismiss();
//            tvClass.setText("Lớp: " + model.getClassName());
//            tvName.setText("Sinh Viên: " + model.getName());
//            tvMssv.setText("MSSV: " + model.getStudentId());
//            tvNghanh.setText("Ngành: " + model.getMajor());
//            data = model.getSchedule();
//            onSuccessSch();
//        }
//
//        return model.getSchedule();
//    }

//    private void onSchedule() {
//        String timeStamp = new SimpleDateFormat("MM/dd/yyyy").format(Calendar.getInstance().getTime());
//        mili = timeStamp;
//        Log.d(TAG, "onSchedule: " + mili);
//        mProgressDialog.show();
//        apiService = ApiClient.getClient(this).create(ApiService.class);
//        String passwordhash = "md5";
//        String fbclid = "IwAR0PCNzGINHaFkXuVtzs9CBybYtZzuDMawmRyUqee8wL-z4xlCNMDruWvTM";
//        compositeDisposable
//                .add(apiService.getLogin(username.toUpperCase(), password, passwordhash, fbclid)
//                        .subscribeOn(Schedulers.io())
//                        .observeOn(AndroidSchedulers.mainThread())
//                        .subscribeWith(new DisposableSingleObserver<Model>() {
//                                           @Override
//                                           public void onSuccess(Model model) {
//                                               mProgressDialog.dismiss();
//                                               tvClass.setText("Lớp: " + model.getClassName());
//                                               tvName.setText("Sinh Viên: " + model.getName());
//                                               tvMssv.setText("MSSV: " + model.getStudentId());
//                                               tvNghanh.setText("Ngành: " + model.getMajor());
//                                               data.addAll(model.getSchedule());
//                                               mAdapter.notifyDataSetChanged();
//                                               onSuccessSch();
//
//                                           }
//
//                                           @Override
//                                           public void onError(Throwable e) {
//
//                                           }
//                                       }
//                        )
//                );
//    }
}
