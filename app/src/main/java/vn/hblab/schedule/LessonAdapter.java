package vn.hblab.schedule;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import vn.hblab.model.Model;
import vn.hblab.skma.R;

public class LessonAdapter extends RecyclerView.Adapter<LessonAdapter.ViewHoler> {

    List<Model.Schedule.Lesson> data;

    public LessonAdapter(List<Model.Schedule.Lesson> data) {
        this.data = data;
    }

    @NonNull
    @Override
    public ViewHoler onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_lesson, parent, false);
        return new ViewHoler(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHoler holder, int position) {
        holder.tvSubjectName.setText(data.get(position).getSubjectName());
        holder.tvAddress.setText("Room "+data.get(position).getAddress());
        holder.tvTimer.setText("Time " + data.get(position).getLesson());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHoler extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_subject_name)
        TextView tvSubjectName;
        @BindView(R.id.tv_address)
        TextView tvAddress;
        @BindView(R.id.tv_timer)
        TextView tvTimer;
        public ViewHoler(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
