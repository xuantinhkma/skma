package vn.hblab.schedule;

import android.content.Context;
import android.graphics.Color;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import vn.hblab.model.Model;
import vn.hblab.skma.R;

public class ScheduleAdapter extends RecyclerView.Adapter<ScheduleAdapter.ViewHolder> {

    private List<Model.Schedule> datas;
    private ListItemClickListener mOnClickListener;
    private Context context;

    public ScheduleAdapter(List<Model.Schedule> datas, ListItemClickListener mOnClickListener, Context context) {
        this.datas = datas;
        this.mOnClickListener = mOnClickListener;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_schedule_layout_test, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(datas.get(position));
        Model.Schedule schedule = datas.get(position);
        if (schedule != null) {
            if(schedule.isClicked()){
                holder.cardViewSc.setCardBackgroundColor(context.getResources().getColor(R.color.orange));
            }else {
                holder.cardViewSc.setCardBackgroundColor(context.getResources().getColor(R.color.white));
            }
        }

        holder.lnItem.setOnClickListener(v -> mOnClickListener.onItemClick(position));
    }

    @Override
    public int getItemCount() {
        return datas.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_date)
        TextView tvDate;
        @BindView(R.id.tv_day)
        TextView tvDay;
        @BindView(R.id.tv_mont)
        TextView tvMont;
        @BindView(R.id.lesson_size)
        TextView lessonSize;
        @BindView(R.id.cardViewSc)
        CardView cardViewSc;
        @BindView(R.id.ln_item)
        LinearLayout lnItem;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(Model.Schedule data) {
            int index = data.getLessons().size();
            String timer;
            int DAY_OF_WEEK = 0;
            String DayOfWeek = "null";

            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            String dateString = convertDate(data.getDate(), "dd-MM-yyyy");
            try {
                Date date = sdf.parse(dateString);
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(date);
                DAY_OF_WEEK = calendar.get(Calendar.DAY_OF_WEEK);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            switch (DAY_OF_WEEK) {
                case 2:
                    DayOfWeek = "Thứ 2";
                    break;
                case 3:
                    DayOfWeek = "Thứ 3";
                    break;
                case 4:
                    DayOfWeek = "Thứ 4";
                    break;
                case 5:
                    DayOfWeek = "Thứ 5";
                    break;
                case 6:
                    DayOfWeek = "Thứ 6";
                    break;
                default:
                    DayOfWeek = "null";
                    break;
            }
            tvDate.setText(DayOfWeek);
            tvDay.setText(convertDate(data.getDate(), "dd"));
            tvMont.setText(convertDate(data.getDate(), "MM"));
            lessonSize.setText("" + index + "");
        }
    }

    public static String convertDate(String dateInMilliseconds, String dateFormat) {
        return DateFormat.format(dateFormat, Long.parseLong(dateInMilliseconds)).toString();
    }

    public interface ListItemClickListener {
        void onItemClick(int position);
    }

}
