package vn.hblab.network;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;
import vn.hblab.model.Model;

import static vn.hblab.network.ApiEndPoint.ENDPOINT_LOG_IN;

public interface ApiService {

    /* login */
    @Headers("Accept: application/json; charset=utf-8")
    @GET(ENDPOINT_LOG_IN)
    Single<Model> getLogin(@Query("username") String username,
                           @Query("password") String password,
                           @Query("passwordhash") String passwordhash,
                           @Query("fbclid") String fbclid);
}
