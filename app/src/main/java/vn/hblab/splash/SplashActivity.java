package vn.hblab.splash;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import io.realm.DynamicRealm;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmMigration;
import io.realm.RealmResults;
import io.realm.RealmSchema;
import vn.hblab.compactCalendar.CalendarActivity;
import vn.hblab.loginmaterial.TestActivity;
import vn.hblab.model.User;
import vn.hblab.schedule.ScheduleActivity;
import vn.hblab.skma.MainActivity;
import vn.hblab.skma.R;

public class SplashActivity extends AppCompatActivity {

    private Realm mRealm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        Realm.init(this);
        RealmConfiguration config = new RealmConfiguration
                .Builder()
                .schemaVersion(0)
                .migration(new MyMigration())
                .build();
        Realm.setDefaultConfiguration(config);
        mRealm = Realm.getDefaultInstance();

        RealmResults<User> user = mRealm.where(User.class).findAll();

        if (user.isEmpty()){
            onLogin();
            finish();
        } else {
            onSchedule();
            finish();
        }
    }

    private void onSchedule() {
        Intent intent = new Intent(this, CalendarActivity.class);
        startActivity(intent);
    }

    private void onLogin() {
        Intent intent = new Intent(this, TestActivity.class);
        startActivity(intent);
    }

    private class MyMigration implements RealmMigration {

        @Override
        public void migrate(@NonNull DynamicRealm realm, long oldVersion, long newVersion) {
            // DynamicRealm exposes an editable schema
            RealmSchema schema = realm.getSchema();
        }
    }
}
